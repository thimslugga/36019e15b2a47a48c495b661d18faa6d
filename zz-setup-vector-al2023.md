# Setup DataDog Vector on Amazon Linux 2023

This is WIP ATM.

- https://github.com/vectordotdev/vector

## Install DataDog Vector for exporting journaled logs to CloudWatch Logs

Either use the setup script or manually add the repo and install the package.

Setup script:

```shell
bash -c "$(curl -L https://setup.vector.dev)"
```

Manually add repo:

```shell
cat <<'EOF' | sudo tee /etc/yum.repos.d/vector.repo
[vector]
name = Vector
baseurl = https://yum.vector.dev/stable/vector-0/$basearch/
enabled=1
gpgcheck=1
repo_gpgcheck=1
priority=1
gpgkey=https://keys.datadoghq.com/DATADOG_RPM_KEY_CURRENT.public
       https://keys.datadoghq.com/DATADOG_RPM_KEY_B01082D3.public
       https://keys.datadoghq.com/DATADOG_RPM_KEY_FD4BF915.public

EOF
```

Install the Vector package:

```shell
sudo dnf install -y vector
```

Backup the default configuration file and then configure the Vector service:

```shell
sudo mv /etc/vector/vector.yaml{,.bak}
```

```shell
cat <<'EOF' | sudo tee /etc/vector/vector.yaml
sources:
  my_journald_source:
    type: "journald"

sinks:
  my_cloudwatch_sink:
    type: "aws_cloudwatch_logs"
    inputs:
      - "my_journald_source"
    compression: "gzip"
    encoding:
      codec: "json"
    #create_missing_group: true
    #create_missing_stream: true
    #endpoint: http://127.0.0.0:5000/path/to/service
    group_name: "prodenv"
    region: "us-east-1"
    stream_name: "prodsite/{{ host }}"

EOF
```

Verify the configuration file is valid:

```shell
vector validate
#vector --config /etc/vector/vector.yaml --require-healthy
```

- https://vector.dev/docs/setup/installation/operating-systems/amazon-linux/
- https://vector.dev/docs/setup/quickstart/
- https://vector.dev/docs/reference/configuration/sinks/aws_cloudwatch_logs/
- https://vector.dev/docs/reference/configuration/sources/journald/
- https://github.com/aws/amazon-cloudwatch-agent/issues/382