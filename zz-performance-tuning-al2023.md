# Performance Tuning for Amazon Linux 2023

## EC2 Bandwidth Limits

```shell
ethtool -S eth0 | grep -E 'err|exceeded|missed'
```

## NIC Tuning

```shell
#sudo ethtool -G eth0 tx 1024 rx 4096
sudo ethtool -G eth0 tx 1024 rx 8192
```

```shellsession
ethtool -c eth0

rx-adaptive on
rx usecs 20
tx usecs 64 (default)
```

```shell
#ethtool -C eth0 adaptive-rx off rx-usecs 0 tx-usecs 0
```

```shell
cat /proc/interrupts | grep Tx-Rx
```

## GRUB Configuration

```shell
uname -sr; cat /proc/cmdline
```

```shell
sudo grubby --update-kernel=ALL --args="intel_idle.max_cstate=1 processor.max_cstate=1 cpufreq.default_governor=performance"
sudo grubby --update-kernel=ALL --args="swapaccount=1 psi=1"
```

Verify:

```shell
sudo grubby --info=ALL
```

To reboot the host:

```shell
sudo systemctl reboot
```

- https://fasterdata.es.net/host-tuning/linux/100g-tuning/cpu-governor/

## sysctl

```shell
# start with 50-70
echo 50 | sudo tee /proc/sys/net/core/busy_read
echo 50 | sudo tee /proc/sys/net/core/busy_poll
```

```shell
echo 0 | sudo tee /proc/sys/net/ipv4/tcp_sack
```

```shell
cat <<'EOF' | sudo tee /etc/sysctl.d/99-custom-tuning.conf
# Custom kernel sysctl configuration file
#
# Disclaimer: These settings are not a one size fits all and you will need to test and validate them in your own environment.
#
# https://www.kernel.org/doc/Documentation/networking/ip-sysctl.txt
# https://www.kernel.org/doc/Documentation/sysctl/net.txt
# https://www.kernel.org/doc/Documentation/networking/proc_net_tcp.txt
# https://www.kernel.org/doc/Documentation/networking/scaling.txt
# https://www.kernel.org/doc/Documentation/networking/multiqueue.txt
# https://www.kernel.org/doc/Documentation/networking/ena.txt
#
# For binary values, 0 is typically disabled, 1 is enabled.
#
# See sysctl(8) and sysctl.conf(5) for more details.
#
# AWS References:
#
# - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-network-bandwidth.html
# - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ena-nitro-perf.html
# - https://github.com/amzn/amzn-drivers/blob/master/kernel/linux/ena/ENA_Linux_Best_Practices.rst
# - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ena-improve-network-latency-linux.html
# - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ena-express.html
# - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/processor_state_control.html
# - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/monitoring-network-performance-ena.html
# - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/install-and-configure-cloudwatch-agent-using-ec2-console.html
# - https://github.com/amzn/amzn-ec2-ena-utilities/tree/main
#
# Misc References:
#
# - https://github.com/leandromoreira/linux-network-performance-parameters
# - https://oxnz.github.io/2016/05/03/performance-tuning-networking/
# - https://www.speedguide.net/articles/linux-tweaking-121
# - https://www.tweaked.io/guide/kernel/
# - http://rhaas.blogspot.co.at/2014/06/linux-disables-vmzonereclaimmode-by.html
# - https://fasterdata.es.net/host-tuning/linux/
# - https://documentation.suse.com/sles/15-SP5/html/SLES-all/cha-tuning-network.html
# - https://blog.packagecloud.io/monitoring-tuning-linux-networking-stack-receiving-data/
# - https://blog.packagecloud.io/monitoring-tuning-linux-networking-stack-sending-data/
# - https://blog.cloudflare.com/optimizing-tcp-for-high-throughput-and-low-latency/
# - https://github.com/myllynen/rhel-performance-guide
# - https://github.com/myllynen/rhel-troubleshooting-guide
# - https://www.brendangregg.com/linuxperf.html

# Adjust the kernel printk to minimize seiral console logging.
# The defaults are very verbose and they can have a performance impact.
# Note: 4 4 1 7 should also be fine, just not debug i.e. 7
kernel.printk=3 4 1 7

# man 7 sched
#
# This feature aimed at improving system responsiveness under load by
# automatically grouping task groups with similar execution patterns.
# While beneficial for desktop responsiveness, in server environments,
# especially those running Kubernetes, this behavior might not always
# be desirable as it could lead to uneven distribution of CPU resources
# among pods. 
# 
# The use of the cgroups(7) CPU controller to place processes in cgroups 
# other than the root CPU cgroup overrides the affect of auto-grouping. 
#
# This setting enables better interactivity for desktop workloads and is 
# not typically suitable for many server type workloads e.g. postgresdb. 
#
# https://cateee.net/lkddb/web-lkddb/SCHED_AUTOGROUP.html
# https://www.postgresql.org/message-id/50E4AAB1.9040902@optionshouse.com
kernel.sched_autogroup_enabled=0

# This affects / allows processes to stay longer on a CPU core since it's 
# last run. This is a heuristic for estimating cache misses e.g. you have 
# a lot tasks to run, for those who still have a lot of their data in cache 
# it's cheaper to wait minimally and then run on the CPU core they last ran
# as cache misses cost quite more cpu-cycle wise.
#
# For those which have not much, or even no, data in the local CPU caches i.e.
# L1, and also L2, it maybe faster/better to just run the task on another CPU 
# with less work i.e. migrate it, as it must re-cache its data anyway.
#
# The heuristic uses, among other things, the time duration since the tasks last 
# run to estimate how many task data is probably still cached, as the longer the 
# task did not run the more likely it is that it's data was evicted from cache to 
# make room for another task. Now, setting this to high can have downsides, e.g.
# cache penalty may add up, but having a too low of setting is also not ideal, as 
# task migration is not exactly free i.e. as often inter-CPU locks must be acquired 
# to move a task to another's CPU cores run queue. But it seems that this values 
# default may be a bit to low for modern systems and a hypervisor work load, so you
# could try to set it to 5ms instead of 0.5 ms, and observe how your system is affected, 
# note that here a higher CPU load which may be desired as basically it just gets used 
# more efficiently i.e. less time wasted in task migrations and you can achieve more 
# throughput.
#
# A lower value e.g. like 500000 (0.5 ms) may improve the responsiveness for certain workloads.
#kernel.sched_migration_cost_ns=500000

# Security feature. No randomization, everything is static.
#kernel.randomize_va_space=1

# For rngd
#kernel.random.write_wakeup_threshold=3072

# Prevent ebpf privilege escalation, see the following:
# https://lwn.net/Articles/742170
# https://www.suse.com/support/kb/doc/?id=000020545
# https://discourse.ubuntu.com/t/unprivileged-ebpf-disabled-by-default-for-ubuntu-20-04-lts-18-04-lts-16-04-esm/27047
# 0 = re-enable, 1 = disable, 2 = disable but allow admin to re-enable without a reboot
#kernel.unprivileged_bpf_disabled=0

# Rootless Containers
# https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md
user.max_user_namespaces=28633

# When set to "enabled", all users are allowed to use userfaultfd syscalls.
# https://lwn.net/Articles/782745/
#vm.unprivileged_userfaultfd=1

# Specifies the minimum number of kilobytes to keep free across the system. 
# This is used to determine an appropriate value for each low memory zone, 
# each of which is assigned a number of reserved free pages in proportion 
# to their size.
#
# Setting min_free_kbytes to an extremely low value prevents the system from 
# reclaiming memory, which can result in system  hangs and OOM-killing processes.
#
# However, setting min_free_kbytes too high e.g. 5–10% of total system memory can 
# cause the system to enter an out-of-memory state immediately, resulting in the
# system spending too much time trying to reclaim memory. 
#
# As a rule of thumb, yset this value to between 1-3% of available system
# memory and adjust this value up or down to meet the needs of your application 
# workload. It is not recommended that the setting of vm.min_free_kbytes 
# exceed 5% of the system's physical memory.
#
# Ensure that the reserved kernel memory is sufficient to sustain a high
# rate of packet buffer allocations as the default value may be too small.
# awk 'BEGIN {OFMT = "%.0f";} /MemTotal/ {print "vm.min_free_kbytes =", $2 * .03;}' /proc/meminfo
vm.min_free_kbytes=1048576

# Maximum number of memory map areas a process may have (memory map areas are used
# as a side-effect of calling malloc, directly by mmap and mprotect, and also when
# loading shared libraries).
vm.max_map_count=262144

vm.overcommit_memory=1

# Make sure the host does not try to swap too early.
# https://access.redhat.com/solutions/6785021
# https://access.redhat.com/solutions/7042476
# vm.force_cgroup_v2_swappiness=1
vm.swappiness=10

# The maximum percentage of dirty system memory.
# https://www.suse.com/support/kb/doc/?id=000017857
vm.dirty_ratio = 10

# Percentage of dirty system memory at which background writeback will start.
# (default 10)
vm.dirty_background_ratio=5

# Some kernels won't allow dirty_ratio to be set below 5%.
# Therefore when dealing with larger amounts of system memory,
# percentage ratios might not be granular enough. If that is the 
# case, then use the below instead of the settings above.
#
# Configure 600 MB maximum dirty cache
#vm.dirty_bytes=629145600

# Spawn background write threads once the cache holds 300 MB
#vm.dirty_background_bytes=314572800

# The value in file-max denotes the maximum number of file-handlers that the Linux kernel will allocate.
# When you get lots of error messages about running out of file handlers, you will want to increase this limit.
# Attempts to allocate more file descriptors than file-max are reported with printk, look for in the kernel logs.
# VFS: file-max limit <number> reached
fs.file-max=1048576

# Maximum number of concurrent asynchronous I/O operations (you might need to
# increase this limit further if you have a lot of workloads that uses the AIO
# subsystem e.g.  MySQL, MariaDB, etc.
# 524288, 1048576, etc.
fs.aio-max-nr=524288

# Upper limit on the number of watches that can be created per real user ID
# Raise the limit for watches to the limit i.e. 524,288
# https://man7.org/linux/man-pages/man7/inotify.7.html
fs.inotify.max_user_watches=524288

# Suppress logging of net_ratelimit callback
#net.core.message_cost=0

# Increasing this value for high speed cards may help prevent losing packets
# https://access.redhat.com/solutions/1241943
net.core.netdev_max_backlog = 2000
net.core.netdev_budget = 600

# The maximum number of "backlogged sockets, accept and syn queues are governed by 
# net.core.somaxconn and net.ipv4.tcp_max_syn_backlog. The maximum number of 
# "backlogged sockets". The net.core.somaxconn setting caps both queue sizes.
# Ensure that net.core.somaxconn is always set to a value equal to or greater than 
# tcp_backlog e.g. net.core.somaxconn >= 4096.
#
# Increase number of incoming connections
net.core.somaxconn = 4096
net.ipv4.tcp_max_syn_backlog = 4096

# Increase UDP Buffers
# Maximum Receive/Transmit Window Size
# if netstat -us is reporting errors, another underlying issue may 
# be preventing the application from draining its receive queue.
# https://github.com/quic-go/quic-go/wiki/UDP-Buffer-Sizes
# https://medium.com/@CameronSparr/increase-os-udp-buffers-to-improve-performance-51d167bb1360
# The maximum allowed (16MB) receive socket buffer (size in bytes)
net.core.rmem_max=16777216
# The maximum allowed (16MB) send socket buffer (size in bytes)
net.core.wmem_max=16777216

# The default socket receive buffer (size in bytes)
#net.core.rmem_default=31457280
#net.core.wmem_default=

# Increase linux auto-tuning of TCP buffer limits to 16MB to prevent dropped packets.
# https://blog.cloudflare.com/the-story-of-one-latency-spike/
net.ipv4.tcp_rmem=4096 87380 16777216
net.ipv4.tcp_wmem=4096 65536 16777216

# Enable busy poll mode
# Busy poll mode reduces latency on the network receive path. When you enable busy poll 
# mode, the socket layer code can directly poll the receive queue of a network device. 
# The downside of busy polling is higher CPU usage in the host that comes from polling 
# for new data in a tight loop. There are two global settings that control the number of 
# microseconds to wait for packets for all interfaces.
# ethtool -k eth0
#net.core.busy_read=50
#net.core.busy_poll=50

# It's recommended to use a 'fair queueing' qdisc e.g. fq or fq_codel.
#
# - fq or fq_codel can be safely used as a drop-in replacement for pfifo_fast.
# - fq or fq_codel is required to use tcp_bbr as it requires fair queuing.
# - fq-codel is best for forwarding/routers which don't originate local traffic,
#   hypervisors and best general purpose qdisc.
# - fq is best for fat servers with tcp-heavy workloads and particularly at 10GigE+.
#
# - BBR supports fq_codel in Linux Kernel version 4.13 and later.
# - BBR must be used with fq qdisc with pacing enabled, since pacing is integral to the BBR design 
#   and implementation. BBR without pacing would not function properly and may incur unnecessary 
#   high packet loss rates.
#
# http://man7.org/linux/man-pages/man8/tc-fq.8.html
# https://github.com/systemd/systemd/blob/main/sysctl.d/50-default.conf
# https://www.bufferbloat.net/projects/codel/wiki/
# https://github.com/systemd/systemd/issues/9725#issuecomment-412286509
# https://forum.vyos.io/t/bbr-and-fq-as-new-defaults/12344
# https://research.google/pubs/pub45646/
# https://github.com/google/bbr/blob/master/README
net.core.default_qdisc = fq_codel
#net.ipv4.tcp_congestion_control = bbr

# Negotiate TCP ECN for active and passive connections
#
# Turn on ECN as this will let AQM sort out the congestion backpressure 
# without incurring packet losses and retransmissions.
#
# In order to make best used of this we really need ECN-enablement 
# sysctl net.ipv4.tcp_ecn on end-hosts.
#
# https://github.com/systemd/systemd/pull/9143
# https://github.com/systemd/systemd/issues/9748
net.ipv4.tcp_ecn = 2
net.ipv4.tcp_ecn_fallback = 1

# Bump the TTL from the default of 64 to 127 on AWS
net.ipv4.ip_default_ttl = 127

# Enable forwarding so that docker networking works as expected.
# Enable IPv4 forwarding
net.ipv4.ip_forward = 1
net.ipv4.conf.all.forwarding = 1
# Enable IPv6 forwarding
net.ipv6.conf.default.forwarding = 1
net.ipv6.conf.all.forwarding = 1

# Disables ICMP redirect sending
net.ipv4.conf.eth0.send_redirects=0
net.ipv4.conf.all.send_redirects=0
net.ipv4.conf.default.send_redirects=0

# Disables ICMP redirect acceptance
net.ipv4.conf.all.accept_redirects=0
net.ipv4.conf.default.accept_redirects=0
net.ipv6.conf.all.accept_redirects=0
net.ipv6.conf.default.accept_redirects=0

net.ipv4.conf.all.secure_redirects=0
net.ipv4.conf.default.secure_redirects=0

# Increase the local outgoing port range
net.ipv4.ip_local_port_range = 10000	65535
#net.ipv4.ip_local_reserved_ports=

# Enable Multipath TCP
net.mptcp.enabled = 1

# Enable low latency mode for TCP, intended to give preference to low latency 
# over higher throughput. Setting to 1 will disable IPv4 tcp pre-queue processing.
#net.ipv4.tcp_low_latency = 1

# Enable TCP Window Scaling
net.ipv4.tcp_window_scaling = 1

# RFC 1323, Support for IPV4 TCP window sizes larger than 64K, which is generally 
# needed on high bandwidth networks. Tells the kernel how much of the socket buffer 
# space should be used for TCP window size and how much to save for an application buffer.
net.ipv4.tcp_adv_win_scale = 1

#net.ipv4.tcp_no_metrics_save = 1

#net.ipv4.tcp_moderate_rcvbuf = 1

# Disable the TCP timestamps option for better CPU utilization.
#net.ipv4.tcp_timestamps = 0

# Recommended for hosts with jumbo frames enabled. Default in AWS.
net.ipv4.tcp_mtu_probing = 1

# Enable to send data in the opening SYN packet.
net.ipv4.tcp_fastopen = 1

# Protect Against TCP Time-Wait Assassination Attacks
net.ipv4.tcp_rfc1337 = 1

# Enable the TCP selective ACKs option for better throughput.
#net.ipv4.tcp_sack = 1

# https://blog.cloudflare.com/optimizing-the-linux-stack-for-mobile-web-per/
# https://access.redhat.com/solutions/168483
# Use this parameter to ensure that the maximum speed is used from beginning
# also for previously idle TCP connections. Avoid falling back to slow start
# after a connection goes idle keeps our cwnd large with the keep alive
# connections (kernel > 3.6).
net.ipv4.tcp_slow_start_after_idle = 0

# The maximum times an IPV4 packet can be reordered in a TCP packet stream without 
# TCP assuming packet loss and going into slow start.
#net.ipv4.tcp_reordering = 3

# The net.ipv4.tcp_tw_recycle option is quite problematic for public-facing servers as it 
# will not handle connections from two different computers behind the same NAT device, which 
# is a problem hard to detect and waiting to bite you in the ass.
#net.ipv4.tcp_tw_recycle=

net.ipv4.tcp_tw_reuse=  1

# Decrease the time default value for connections to keep alive.
#net.ipv4.tcp_keepalive_time = 300
#net.ipv4.tcp_keepalive_probes = 5
#net.ipv4.tcp_keepalive_intvl = 15

# Decrease the time default value for tcp_fin_timeout connection, FIN-WAIT-2
#net.ipv4.tcp_fin_timeout = 15

# Reduce TIME_WAIT from the 120s default to 30-60s
#net.netfilter.nf_conntrack_tcp_timeout_time_wait = 30

# Reduce FIN_WAIT from teh 120s default to 30-60s
#net.netfilter.nf_conntrack_tcp_timeout_fin_wait = 30

EOF
```

To apply these settings:

```shell
sudo systemctl daemon-reload
sudo sysctl --system
```