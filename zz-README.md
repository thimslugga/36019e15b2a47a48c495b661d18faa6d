# Unoffical Guide to Amazon Linux 2023

## Amazon Linux 2023 Resources

- https://aws.amazon.com/linux/amazon-linux-2023/faqs/
- https://github.com/amazonlinux/amazon-linux-2023/
- https://cdn.amazonlinux.com/al2023/os-images/latest/
- https://alas.aws.amazon.com/alas2023.html
- https://docs.aws.amazon.com/linux/al2023/
- https://docs.aws.amazon.com/linux/al2023/release-notes/relnotes.html
- https://docs.aws.amazon.com/linux/al2023/ug/deterministic-upgrades.html
- [Manage package and operating system updates in AL2023](https://docs.aws.amazon.com/linux/al2023/ug/managing-repos-os-updates.html)
- https://lwn.net/Articles/926352/

### AL2023 Repository Details

```
# cdn.amazonlinux.com (x86_64)
https://cdn.amazonlinux.com/al2023/core/mirrors/latest/x86_64/mirror.list
https://cdn.amazonlinux.com/al2023/core/guids/<guid>/x86_64/

# cdn.amazonlinux.com (aarch64)
https://cdn.amazonlinux.com/al2023/core/mirrors/latest/aarch64/mirror.list
https://cdn.amazonlinux.com/al2023/core/guids/<guid>/aarch64/

# al2023-repos-us-east-1-<guid>.s3.dualstack.<region>.amazonaws.com
https://al2023-repos-<region>-<guid>.s3.dualstack.<region>.amazonaws.com/core/mirrors/<releasever>/x86_64/mirror.list
https://al2023-repos-<region>-<guid>.s3.dualstack.<region>.amazonaws.com/core/guids/<guid>/x86_64/<rest_of_url>
https://al2023-repos-<region>-<guid>.s3.dualstack.<region>.amazonaws.com/core/mirrors/<releasever>/SRPMS/mirror.list
https://al2023-repos-<region>-<guid>.s3.dualstack.<region>.amazonaws.com/kernel-livepatch/mirrors/al2023/x86_64/mirror.list
```

## Docker Resources

- https://mobyproject.org/
- https://github.com/docker/docker-install
- https://github.com/docker/docker-ce-packaging
- https://download.docker.com/linux/static/stable/
- https://docs.docker.com/compose/install/linux/
- https://github.com/docker/compose/
- https://github.com/docker/docker-credential-helpers
- https://github.com/docker/buildx

## Containers Resources

* https://gallery.ecr.aws/