#!/bin/bash

# https://brew.sh/
# https://docs.brew.sh/Homebrew-on-Linux#install

sudo dnf groupinstall 'Development Tools'
sudo dnf install --alowerasing -y procps-ng curl file git git-lfs

# set password as homebrew script wont allow running as root i.e. sudo
sudo passwd ec2-user

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# add to bash env
(echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> /home/ec2-user/.bashrc
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# Ensure `/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin` is in your PATH
echo $PATH

# View formulae details about the homebrew packaage:
#brew info jq

# install packages via homebrew:
#brew install gcc